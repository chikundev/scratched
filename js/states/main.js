// chikun :: 2014
// Main menu state


states.main = {

    create: function () {

        // Set current music object
        currentMusic = document.getElementById("main");
        currentMusic.volume = 1;

        // Play background music
        currentMusic.play();

    },

    update: function (dt) {
    },

    kill: function () {

        // Stop background music
        currentMusic.pause();

    }
};
