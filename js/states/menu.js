// chikun :: 2014
// Menu state


states.menu = {

    create: function () {


        // Keeps track of time for the fade-in
        this.timer = 1;

        // Whether we have control of menu
        this.haveControl = false;

        // Create the fade screen
        obj.fade = new PIXI.Graphics();
        obj.fade.beginFill(0x000000);
        obj.fade.drawRect(0, 0, 320, 240);

        // Create the fade screen
        obj.fae = new PIXI.Graphics();
        obj.fae.beginFill(0xFFFFFF);
        obj.fae.drawRect(0, 0, 320, 240);

        // Add objects to screen
        addChild(obj.fade, -1000);
        addChild(obj.fae, 0);


        // Set current music object
        currentMusic = document.getElementById("bgmMain");
        currentMusic.volume = 0;

        // Play background music
        currentMusic.play();


    },

    update: function (dt) {

        // Count down the fade timer
        this.timer = Math.max(this.timer - dt, 0);

        // If time has elapsed
        if (this.timer <= 0) {

            this.haveControl = true;

        }

        // Calculate alpha of the logo
        obj.fade.alpha = this.timer;

        // Update music volume
        currentMusic.volume = 1 - this.timer;

    },

    kill: function () {

        // Detach all images from screen
        container.removeChildren();

        // Delete all objects
        obj = { };

        // Stop current music
        currentMusic.pause();

    }
};
