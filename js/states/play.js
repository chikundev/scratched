// chikun :: 2014
// Main game loop for Scratched


states.play = {
    // On state creation
    create: function () {
        // Set song
        song = {
            bpm:    138,
            name:   "Level 3",
            blocks: [
                [ 0, 0, 1, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 1, 0, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 0, 1, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 1, 0, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 0, 1, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 1, 0, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 0, 1, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 1, 0, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 0, 1, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 1, 0, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 0, 1, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 1, 0, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 0, 1, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 1, 0, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 0, 1, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 1, 0, 0 ],
                [ 1, 1, 0, 0 ],
                [ 0, 0, 1, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 1, 0, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 0, 1, 0 ],
                [ 1, 0, 0, 0 ],
                [ 0, 1, 0, 0 ],
                [ 1, 0, 0, 0 ],
            ]
        };

        // Set timer
        timer = 0;

        // Add background to canvas
        obj.bg = makeSprite(tex.bg, 0, 0, 0, 0, 10, 10);
        container.addChild(obj.bg);

        // Add record to canvas
        obj.record = makeSprite(tex.record, 280, 200, 0.5, 0.5);
        container.addChild(obj.record);

        // Add logo to canvas
        obj.logo = makeSprite(tex.logo, 160, 12, 0.5, 0);
        container.addChild(obj.logo);

        var blocksNum = song.blocks.length;
        obj.blocks = [ ];
        for (var x = 0; x < blocksNum; x++) {
            for (var y = 0; y < 4; y++) {
                if (song.blocks[x][y]) {
                    var tmp = makeSprite(tex.block, 24 * x, 64 +
                        (24 * y));
                    obj.blocks.push(tmp);
                    container.addChild(obj.blocks[obj.blocks.length - 1]);
                }
            }
        }

        // Set current music object
        currentMusic = document.getElementById("bgmLevel");
        currentMusic.volume = 1;
        currentMusic.play();

        obj.bpmText = new PIXI.Text("BPM: " + song.bpm);
        obj.bpmText.position.x = 5;
        obj.bpmText.position.y = 210;
        container.addChild(obj.bpmText);

    },

    // On state update
    update: function (dt) {

        // Update timer, print if above beat
        var beatInterval = (60 / song.bpm);
        timer += dt;
        if (timer >= beatInterval) {
            timer -= beatInterval;
            console.log("BEAT" + Date.now());
        }

        for (var i = 0; i < obj.blocks.length; i++) {
            obj.blocks[i].position.x -= dt / beatInterval * 48;
        }

        // Rotate record
        obj.record.rotation += 0.1;

        if (input.check(key.jump)) {
            console.log("HALLELUJAH");
        }

        obj.record.position.x += (input.check(key.moveR) -
                                  input.check(key.moveL)) * 256 * dt;
        obj.record.position.y += (input.check(key.moveD) -
                                  input.check(key.moveU)) * 256 * dt;

    },

    kill: function () {

        // Remove sprites from container display
        container.removeChildren();

        // Delete all game objects
        obj = { };
        song = null;

    },

    keypressed: function (key) {

        console.log(key);

    }
};
