// chikun :: 2014
// Splash state


states.splash = {

    create: function () {

        // Keeps track of time for the splash
        this.timer = 1;

        // Whether the intro blip has played or not
        this.blipPlayed = false;

        // Create the logo image
        obj.logo = makeSprite(tex.splash, 0, 0);

        // Add image objects to screen
        addChild(obj.logo, 0);

    },

    update: function (dt) {

        // Count down the splash timer
        this.timer -= dt;

        // If time has elapsed
        if (this.timer <= 0) {

            // Move to -menu-
            state.changeTo("menu");

        }
        // Otherwise check if below certain time and the blip had played
        else if (this.timer <= 0.5 && !this.blipPlayed) {

            // Play said blip
            document.getElementById("blip").play();

            // Let program know blip has been played
            this.blipPlayed = true;

        }

        // Calculate alpha of the logo
        obj.logo.alpha = 1 - (Math.abs(this.timer - 0.5) * 2);

    },

    kill: function () {

        // Detach all images from screen
        container.removeChildren();

        // Delete all objects
        obj = { };

    }
};
