// chikun :: 2014
// Load all resources required for game

// GET PXLOADER INSTEAD OF PRELOADJS

var states = { };   // Holds all states

// Set base URL for requirejs
requirejs.config({
    baseUrl: 'js',
});


// Libraries to load concurrently
var libs = [
    'lib/preloadjs-0.4.1.min',
    'states/main',
    'states/menu',
    'states/play',
    'states/splash',
    'input',
    'stateManager',
    'textureManager'
];


// Load our libraries
requirejs(libs, function () {
    // Load our game
    requirejs(['main'], function () {
        // GET THIS SHIT STARTED
        game.load();
    });
});
