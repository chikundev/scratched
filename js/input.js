/*global THREE*/
/*jslint sloppy: true*/
// chikun :: 2014
// Handles all input

var input, pressed, key;


// Contains all input functions
input = { };

// Contains currently pressed keys
pressed = { };

// Table of keys
key = {
    moveL:  37,
    moveR:  39,
    moveU:  38,
    moveD:  40,
    jump:   32
};


// Run at game initialisation
input.init = function () {

    // Set up input handlers
    document.body.onkeydown = function (e) {

        e = e || window.event;
        document.title = " Keypress: " + e.keyCode;
        pressed[e.keyCode] = true;

        // Fire state function if exists
        if (typeof states[state.current].keypressed !== 'undefined') {

            states[state.current].keypressed(e.keyCode);

        }
    };
    document.body.onkeyup = function (e) {

        e = e || window.event;
        delete pressed[e.keyCode];

        // Fire state function if exists
        if (typeof states[state.current].keyreleased !== 'undefined') {

            states[state.current].keyreleased(e.keyCode);

        }
    };
    document.body.onblur = function () {

        pressed = { };

    };
};


// Check if input is pressed
input.check = function (num) {

    return (pressed[num] || false);

};
