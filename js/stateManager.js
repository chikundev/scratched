// chikun : 2014
// Manager for all states in this game


var state = {   // Holds all state functions
    current: "splash"
};


// Loads a state
state.load = function (newState) {

    // Set current state to newState
    state.current = newState;

    // Run create command on the state
    states[state.current].create();

};


// Changes to a state
state.changeTo = function (newState) {

    // Kill current state
    states[state.current].kill();

    // Load new state
    state.load(newState);

};


// Updates the main state
state.update = function (dt) {

    // Pass delta time to state update function
    state.current.update(dt);

};
