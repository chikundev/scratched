// chikun :: 2014
// Main game loop for Scratched


// Global variables which are to be used all across this game
var game, stage, renderer, currentMusic, slowMusic,
    container, obj, song, bpmText, timer, lastUpdate;


// Holds all game functions
game = { };


// Last update time
lastUpdate = Date.now();


// Holds all graphics and textures
obj = { };


// On game creation
game.load = function () {

    // Set up input
    input.init();

    // Create our game stage
    stage = new PIXI.Stage(0);

    // Create a renderer instance
    renderer = PIXI.autoDetectRenderer(640, 480);

    // Insert game view into gameBox div
    var gameBox = document.getElementById("gameBox");
    gameBox.appendChild(renderer.view);

    // Create container for objects
    container = new PIXI.DisplayObjectContainer();
    container.scale.x = container.scale.y = 2;
    stage.addChild(container);

    // Start chain of game.update
    requestAnimFrame(game.update);

    // Create the state
    states[state.current].create();

};


// On game update
game.update = function () {

    // Ensure game.update gets run next update
    requestAnimFrame(game.update);

    // Get delta time
    var now = Date.now();
    var dt = (now - lastUpdate) / 1000;
    lastUpdate = now;

    // Update current state
    states[state.current].update(dt);

    // Find div which game is inside of
    var gameBox = document.getElementById("gameBox");

    // Change game scale based on div
    container.scale.x = (gameBox.clientWidth / 320);
    container.scale.y = (gameBox.clientHeight / 240);

    // Draw the screen
    renderer.render(stage);

};
