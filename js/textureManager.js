// chikun :: 2014
// Loads all textures used in-game


// Set default scaling mode
PIXI.scaleModes.DEFAULT = PIXI.scaleModes.NEAREST;


// Load textures to a table
var tex = {
    bg:     PIXI.Texture.fromImage("gfx/bg.png"),
    block:  PIXI.Texture.fromImage("gfx/block.png"),
    dots:   PIXI.Texture.fromImage("gfx/dots.png"),
    logo:   PIXI.Texture.fromImage("gfx/gameLogo.png"),
    record: PIXI.Texture.fromImage("dev/gfx/recordCircle.png"),
    splash: PIXI.Texture.fromImage("gfx/splash.png")
};


// Create new sprite and return it
function makeSprite(texture, x, y, anchorX, anchorY, scaleX, scaleY) {

    // New sprite
    var tmpSprite = new PIXI.Sprite(texture);

    // Set position if provided
    tmpSprite.position.x = (x || 0);
    tmpSprite.position.y = (y || 0);

    // Set anchor if provided
    tmpSprite.anchor.x = (anchorX || 0);
    tmpSprite.anchor.y = (anchorY || 0);

    // Set scale if provided
    tmpSprite.scale.x = (scaleX || 1);
    tmpSprite.scale.y = (scaleY || 1);

    // Return new sprite
    return (tmpSprite);

}


// Adds an object to the container at a certain depth
function addChild(child, depth) {

    // Position at which we will store child in the container.children array
    var pos = 0;

    // Depth to place child at -- specified value or 0
    depth = (depth || 0);

    // Iterate through all current children
    for (var i = 0; i < container.children.length; i++) {

        // If child is a higher depth
        if (container.children[i].depth >= depth) {

            // Put self in front of child
            pos = i + 1;

        }
    }

    // Give child its specified depth
    child.depth = depth;

    // Insert child at calculated position
    container.addChildAt(child, pos);

}
